<!DOCTYPE node PUBLIC "-//freedesktop//DTD D-BUS Object Introspection 1.0//EN" "http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd">
<node name="/" xmlns:tp="http://telepathy.freedesktop.org/wiki/DbusSpec#extensions-v0">
  <tp:enum name="VM_STATE" type="s">
    <tp:docstring>VM lifecycle state codes.</tp:docstring>
    <tp:enumvalue suffix="CREATING" value="creating"/>
    <tp:enumvalue suffix="RUNNING" value="running"/>
    <tp:enumvalue suffix="STOPPING" value="stopping"/>
    <tp:enumvalue suffix="STOPPED" value="stopped"/>
    <tp:enumvalue suffix="LOCKED" value="locked"/>
    <tp:enumvalue suffix="REBOOTING" value="rebooting"/>
    <tp:enumvalue suffix="REBOOTED" value="rebooted"/>
    <tp:enumvalue suffix="SUSPENDING" value="suspending"/>
    <tp:enumvalue suffix="SUSPENDED" value="suspended"/>
    <tp:enumvalue suffix="RESTORING" value="restoring"/>
    <tp:enumvalue suffix="PAUSED" value="paused"/>
  </tp:enum>

  <tp:enum name="MANAGED_DISKTYPE" type="s">
    <tp:docstring>Managed disk type.</tp:docstring>
    <tp:enumvalue suffix="NONE" value=""/>
    <tp:enumvalue suffix="SYSTEM" value="system"/>
    <tp:enumvalue suffix="APPLICATION" value="application"/>
    <tp:enumvalue suffix="USER" value="user"/>
  </tp:enum>

  <tp:enum name="S3_MODE" type="s">
    <tp:docstring>S3 mode.</tp:docstring>
    <tp:enumvalue suffix="PV" value="pv"/>
    <tp:enumvalue suffix="IGNORE" value="ignore"/>
    <tp:enumvalue suffix="RESTART" value="restart"/>
    <tp:enumvalue suffix="SNAPSHOT" value="snapshot"/>
  </tp:enum>

  <tp:enum name="S4_MODE" type="s">
    <tp:docstring>S4 mode.</tp:docstring>
    <tp:enumvalue suffix="PV" value="pv"/>
    <tp:enumvalue suffix="IGNORE" value="ignore"/>
    <tp:enumvalue suffix="RESTART" value="restart"/>
    <tp:enumvalue suffix="SNAPSHOT" value="snapshot"/>
  </tp:enum>

  <interface name="com.citrix.xenclient.xenmgr">
    <tp:docstring>Main xenmgr interface, used for VM creation and enumeration.</tp:docstring>

    <method name="list_vms">
        <tp:docstring>List each VM present. Returns a list of dicts with few critical properties filled for each VM, including VM state, domain ID (if running), uuid etc.</tp:docstring>
        <arg name="paths" type="ao" direction="out"/>
    </method>

    <method name="find_vm_by_uuid">
      <tp:docstring>Returns the object path to the VM with the given UUID, or raises an error.</tp:docstring>
      <arg name="uuid" type="s" direction="in"/>
      <arg name="obj_path" type="o" direction="out"/>
    </method>

    <method name="find_vm_by_domid">
      <tp:docstring>Returns the object path to the VM of the given domain ID. Fails with an error if no such VM is running.</tp:docstring>
      <arg name="domid" type="i" direction="in"/>
      <arg name="obj_path" type="o" direction="out"/>
    </method>

    <signal name="vm_config_changed">
      <tp:docstring>Notify that VM configuration has changed.</tp:docstring>
      <arg name="uuid" type="s"/>
      <arg name="obj_path" type="o"/>
    </signal>

    <signal name="vm_state_changed">
      <tp:docstring>Notify that VM state has changed.</tp:docstring>
      <arg name="uuid" type="s"/>
      <arg name="obj_path" type="o"/>
      <arg name="state" type="s"/>
      <arg name="acpi_state" type="i"/>
    </signal>

    <signal name="vm_name_changed">
      <tp:docstring>Notify that VM name has changed.</tp:docstring>
      <arg name="uuid" type="s"/>
      <arg name="obj_path" type="o"/>
    </signal>

    <signal name="config_changed">
      <tp:docstring>Notify xenmgr that host level configuration has changed.</tp:docstring>
    </signal>

    <signal name="language_changed">
      <tp:docstring>Notify that the language settings have changed.</tp:docstring>
    </signal>

    <signal name="vm_created">
      <tp:docstring>Notify that a VM was created.</tp:docstring>
      <arg name="uuid" type="s"/>
      <arg name="obj_path" type="o"/>
    </signal>

    <signal name="vm_deleted">
      <tp:docstring>Notify that a VM was deleted.</tp:docstring>
      <arg name="uuid" type="s"/>
      <arg name="obj_path" type="o"/>
    </signal>

    <signal name="network_state_changed">
      <tp:docstring>Notify when a network becomes available/unavailable.</tp:docstring>
      <arg name="available" type="b"/>
    </signal>

    <signal name="vm_transfer_changed">
      <tp:docstring>VM download/upload progress has changed.</tp:docstring>
      <arg name="uuid" type="s"/>
      <arg name="obj_path" type="o"/>
    </signal>

    <signal name="cd_assignment_changed">
      <tp:docstring>CD device VM assignment has changed.</tp:docstring>
      <arg name="dev" type="s"/>
      <arg name="uuid" type="s"/>
      <arg name="obj_path" type="o"/>
    </signal>
  </interface>
</node>
