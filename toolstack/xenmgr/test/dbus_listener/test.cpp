//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <dbus_helpers.h>
#include <dbus_listener.h>
#include <hippomocks.h>
#include <iostream>
#include <unittest.h>

// NoDelete share_ptr
template <class T>
void
NoDelete(T *)
{
}

TEST_CASE("UUID to VM PATH", "[helpers]")
{
    uuid_t uuid("00000000-0000-0000-0000-000000000001");

    REQUIRE(uuid.toString() == "{00000000-0000-0000-0000-000000000001}");
    REQUIRE(uuid_to_object_path(uuid) == "/vm/00000000_0000_0000_0000_000000000001");
}

TEST_CASE("VM PATH to UUID", "[helpers]")
{
    QDBusObjectPath path("/vm/00000000_0000_0000_0000_000000000001");

    REQUIRE(object_path_to_uuid(path.path()).toString() == "{00000000-0000-0000-0000-000000000001}");
}

TEST_CASE("Validate Colors", "[helpers]")
{
    REQUIRE(is_valid_color("#FFFFFF") == true);
    REQUIRE(is_valid_color("#FFF") == true);
    REQUIRE(is_valid_color("white") == true);
    REQUIRE(is_valid_color("#fff") == true);

    REQUIRE(is_valid_color("#FFFFFFF") == false);
    REQUIRE(is_valid_color("FFFFFF") == false);
    REQUIRE(is_valid_color("offwhite") == false);
    REQUIRE(is_valid_color("") == false);
}

TEST_CASE("DBUS getters", "[dbus]")
{
    MockRepository mocks;
    std::shared_ptr<xenmgr_dbus_t> dbus(mocks.Mock<xenmgr_dbus_t>(), NoDelete<xenmgr_dbus_t>);
    std::shared_ptr<xenmgr_host_dbus_t> host(mocks.Mock<xenmgr_host_dbus_t>(), NoDelete<xenmgr_host_dbus_t>);
    std::shared_ptr<xenmgr_vm_dbus_t> vm(mocks.Mock<xenmgr_vm_dbus_t>(), NoDelete<xenmgr_vm_dbus_t>);
    std::shared_ptr<QDBusConnection> conn(mocks.Mock<QDBusConnection>(), NoDelete<QDBusConnection>);

    mocks.OnCallFunc(seam::qobj_connect); // Nothing to check here

    mocks.ExpectCallFunc(get_system_bus).Return(conn);
    mocks.ExpectCallFunc(make_xenmgr_dbus).Return(dbus);
    mocks.ExpectCallFunc(make_xenmgr_host_dbus).Return(host);
    mocks.ExpectCallFunc(qdbus_is_connected).Return(true);
    dbus_listener_t listener;

    uuid_t uivm_uuid("00000000-0000-0000-0000-000000000001");
    QDBusObjectPath uivm_path("/vm/00000000_0000_0000_0000_000000000001");

    uuid_t bad_uuid("00000000-0000-0000-0000-000000000000");
    QDBusObjectPath bad_path("/vm/00000000_0000_0000_0000_000000000000");

    uuid_t off_uuid("00000000-0000-0000-0000-000000000002");
    QDBusObjectPath off_path("/vm/00000000_0000_0000_0000_000000000002");

    mocks.OnCallFunc(make_xenmgr_vm_dbus).Return(vm);

    mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::vglass_enabled).Return(true);
    listener.vm_state_changed("00000000-0000-0000-0000-000000000001", uivm_path, "creating", 0);
    mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::vglass_enabled).Return(true);
    listener.vm_state_changed("00000000-0000-0000-0000-000000000001", uivm_path, "running", 0);
    mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::vglass_enabled).Return(true);
    listener.vm_state_changed("00000000-0000-0000-0000-000000000001", uivm_path, "stopped", 5);

    SECTION("Fail to get system bus")
    {
        std::shared_ptr<QDBusConnection> null(nullptr);
        mocks.ExpectCallFunc(get_system_bus).Return(null);
        mocks.ExpectCallFunc(make_xenmgr_dbus).Return(dbus);
        mocks.ExpectCallFunc(make_xenmgr_host_dbus).Return(host);
        REQUIRE_THROWS(dbus_listener_t listener_fail);
    }
    mocks.OnCallFunc(get_system_bus).Return(conn);

    SECTION("Fail to create xenmgr_dbus")
    {
        std::shared_ptr<xenmgr_dbus_t> null(nullptr);
        mocks.ExpectCallFunc(make_xenmgr_dbus).Return(null);
        mocks.ExpectCallFunc(make_xenmgr_host_dbus).Return(host);
        REQUIRE_THROWS(dbus_listener_t listener_fail);
    }
    mocks.OnCallFunc(make_xenmgr_dbus).Return(dbus);

    SECTION("Fail to create xenmgr_host_dbus")
    {
        std::shared_ptr<xenmgr_host_dbus_t> null(nullptr);
        mocks.ExpectCallFunc(make_xenmgr_host_dbus).Return(null);
        REQUIRE_THROWS(dbus_listener_t listener_fail);
    }
    mocks.OnCallFunc(make_xenmgr_host_dbus).Return(host);

    SECTION("Fail to connect")
    {
        mocks.ExpectCallFunc(qdbus_is_connected).Return(false);
        REQUIRE_THROWS(dbus_listener_t listener_fail);
    }
    mocks.OnCallFunc(qdbus_is_connected).Return(true);

    SECTION("Get vm_list")
    {
        QDBusPendingReply<qlist_t<QDBusObjectPath>> error_reply(QDBusMessage::createError(QDBusError::Other, "Test Error"));
        mocks.OnCall(dbus.get(), xenmgr_dbus_t::list_vms).Return(error_reply);
        REQUIRE_THROWS(listener.get_vm_list());

        qlist_t<QDBusObjectPath> vm_list;
        vm_list << uivm_path;
        vm_list << off_path;
        vm_list << bad_path;

        qlist_t<uuid_t> test_list;
        test_list << uivm_uuid;

        mocks.ExpectCallFunc(pending_reply_is_error).Return(false);
        mocks.ExpectCallFunc(pending_reply_value).Return(vm_list);

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::vglass_enabled).Return(true);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::domid).Return(1);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::vglass_enabled).Return(true);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::domid).Return(-1);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::domid).Return(-1);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::vglass_enabled).Return(false);

        REQUIRE(listener.get_vm_list() == test_list);
    }

    SECTION("Get domid")
    {
        REQUIRE_THROWS(listener.get_domid(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::domid).Return(DOMID_FIRST_RESERVED);
        REQUIRE_THROWS(listener.get_domid(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::domid).Return(5);
        REQUIRE(listener.get_domid(uivm_uuid) == 5);
    }

    SECTION("Get stub_domid")
    {
        REQUIRE_THROWS(listener.get_stub_domid(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::stub_domid).Return(DOMID_FIRST_RESERVED);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::stubdom).Return(true);
        REQUIRE_THROWS(listener.get_stub_domid(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::stub_domid).Return(-1);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::stubdom).Return(false);
        REQUIRE(listener.get_stub_domid(uivm_uuid) == 65535);

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::stub_domid).Return(5);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::stubdom).Return(true);
        REQUIRE(listener.get_stub_domid(uivm_uuid) == 5);
    }

    SECTION("Get name")
    {
        REQUIRE_THROWS(listener.get_name(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::name).Return("test");
        REQUIRE(listener.get_name(uivm_uuid) == "test");
    }

    SECTION("Get image_path")
    {
        REQUIRE_THROWS(listener.get_image_path(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::image_path_vg).Return("test.cpp");
        mocks.ExpectCallFunc(icon_is_null).Return(true);
        REQUIRE(listener.get_image_path(uivm_uuid) == default_image_path);

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::image_path_vg).Return("test.png");
        mocks.ExpectCallFunc(icon_is_null).Return(false);
        REQUIRE(listener.get_image_path(uivm_uuid) == QString("test.png").prepend(image_path_prepend));
    }

    SECTION("Get long_form")
    {
        REQUIRE_THROWS(listener.get_long_form(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::long_form_vg).Return("UNCONFIGURED");
        REQUIRE(listener.get_long_form(uivm_uuid) == "UNCONFIGURED");
    }

    SECTION("Get short_form")
    {
        REQUIRE_THROWS(listener.get_short_form(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::short_form_vg).Return("UNCONFIGURED");
        REQUIRE(listener.get_short_form(uivm_uuid) == "UNCONFIGURED");
    }

    SECTION("Get text_color")
    {
        REQUIRE_THROWS(listener.get_text_color(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::text_color_vg).Return("");
        REQUIRE(listener.get_text_color(uivm_uuid) == "#000000");

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::text_color_vg).Return("#CCCCCC");
        REQUIRE(listener.get_text_color(uivm_uuid) == "#CCCCCC");
    }

    SECTION("Get border_width")
    {
        REQUIRE_THROWS(listener.get_border_width(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::border_width_vg).Return(-1);
        REQUIRE_THROWS(listener.get_border_width(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::border_width_vg).Return(0);
        REQUIRE(listener.get_border_width(uivm_uuid) == 0);
    }

    SECTION("Get border_height")
    {
        REQUIRE_THROWS(listener.get_border_height(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::border_height_vg).Return(-1);
        REQUIRE_THROWS(listener.get_border_height(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::border_height_vg).Return(0);
        REQUIRE(listener.get_border_height(uivm_uuid) == 0);
    }

    SECTION("Get stubdom")
    {
        REQUIRE_THROWS(listener.get_stubdom(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::stubdom).Return(true);
        REQUIRE(listener.get_stubdom(uivm_uuid) == true);
    }

    SECTION("Get slot")
    {
        REQUIRE_THROWS(listener.get_slot(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::slot).Return(-1);
        REQUIRE_THROWS(listener.get_slot(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::slot).Return(0);
        REQUIRE(listener.get_slot(uivm_uuid) == 0);
    }

    SECTION("Get gpu")
    {
        REQUIRE_THROWS(listener.get_gpu(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::gpu).Return("");
        REQUIRE(listener.get_gpu(uivm_uuid) == "");
    }

    SECTION("Get mosaic_mode")
    {
        REQUIRE_THROWS(listener.get_mosaic_mode(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::mosaic_mode).Return(-1);
        REQUIRE_THROWS(listener.get_mosaic_mode(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::mosaic_mode).Return(8);
        REQUIRE_THROWS(listener.get_mosaic_mode(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::mosaic_mode).Return(0);
        REQUIRE(listener.get_mosaic_mode(uivm_uuid) == 0);
    }

    SECTION("Get x")
    {
        REQUIRE_THROWS(listener.get_x(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_x).Return(-1);
        REQUIRE_THROWS(listener.get_x(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_x).Return(0);
        REQUIRE(listener.get_x(uivm_uuid) == 0);
    }

    SECTION("Get y")
    {
        REQUIRE_THROWS(listener.get_y(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_y).Return(-1);
        REQUIRE_THROWS(listener.get_y(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_y).Return(0);
        REQUIRE(listener.get_y(uivm_uuid) == 0);
    }

    SECTION("Get width")
    {
        REQUIRE_THROWS(listener.get_width(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_w).Return(-1);
        REQUIRE_THROWS(listener.get_width(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_w).Return(0);
        REQUIRE(listener.get_width(uivm_uuid) == 0);
    }

    SECTION("Get height")
    {
        REQUIRE_THROWS(listener.get_height(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_h).Return(-1);
        REQUIRE_THROWS(listener.get_height(uivm_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::windowed_h).Return(0);
        REQUIRE(listener.get_height(uivm_uuid) == 0);
    }

    SECTION("Get primary_domain_color")
    {
        REQUIRE_THROWS(listener.get_primary_domain_color(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::primary_domain_color).Return("");
        REQUIRE(listener.get_primary_domain_color(uivm_uuid) == "#FFFFFF");

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::primary_domain_color).Return("#CCCCCC");
        REQUIRE(listener.get_primary_domain_color(uivm_uuid) == "#CCCCCC");
    }

    SECTION("Get secondary_domain_color")
    {
        REQUIRE_THROWS(listener.get_secondary_domain_color(bad_uuid));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::secondary_domain_color).Return("");
        REQUIRE(listener.get_secondary_domain_color(uivm_uuid) == "#FFFFFF");

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::secondary_domain_color).Return("#CCCCCC");
        REQUIRE(listener.get_secondary_domain_color(uivm_uuid) == "#CCCCCC");
    }

    SECTION("Set rect")
    {
        REQUIRE_THROWS(listener.set_rect(bad_uuid, QRect(0,0,0,0)));

        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::setWindowed_x);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::setWindowed_y);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::setWindowed_w);
        mocks.ExpectCall(vm.get(), xenmgr_vm_dbus_t::setWindowed_h);
        REQUIRE_NOTHROW(listener.set_rect(uivm_uuid, QRect(0,0,0,0)));
    }


    SECTION("Remove vm")
    {
        REQUIRE_THROWS(listener.vm_deleted("00000000-0000-0000-0000-000000000000", bad_path));

        REQUIRE_NOTHROW(listener.vm_deleted("00000000-0000-0000-0000-000000000001", uivm_path));
        REQUIRE_THROWS(listener.vm_deleted("00000000-0000-0000-0000-000000000001", uivm_path));
    }

    SECTION("Vm name changed")
    {
        REQUIRE_NOTHROW(listener.vm_name_changed("00000000-0000-0000-0000-000000000001", uivm_path));
        REQUIRE_NOTHROW(listener.vm_name_changed("00000000-0000-0000-0000-000000000000", bad_path));
    }

    SECTION("Host vals")
    {
        mocks.ExpectCall(host.get(), xenmgr_host_dbus_t::mosaic_enabled).Return(true);
        REQUIRE( listener.get_mosaic_enabled() == true );

        mocks.ExpectCall(host.get(), xenmgr_host_dbus_t::display_configurable).Return(true);
        REQUIRE( listener.get_display_configurable() == true );
    }
}
