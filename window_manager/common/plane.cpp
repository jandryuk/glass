//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <plane.h>

uint
qHash(rect_t rect)
{
    int32_t x0 = rect.topLeft().x();
    int32_t y0 = rect.topLeft().y();

    int32_t x1 = rect.bottomRight().x();
    int32_t y1 = rect.bottomRight().y();

    uint64_t i1 = ((uint64_t) x0 << 32) | (x1);
    uint64_t i2 = ((uint64_t) y0 << 32) | (y1);

    return qHash(qHash(i1) ^ qHash(i2));
}

plane_t::plane_t(rect_t rect, point_t plane_origin) : m_plane(rect), m_plane_origin(plane_origin) {}

rect_t
plane_t::parent_rect()
{
    return rect_t(origin(), rect().size());
}

rect_t
plane_t::rect()
{
    return m_plane.boundingRect();
}

void
plane_t::set_rect(rect_t rect)
{
  m_plane &= rect_t();

  m_plane += rect;
  set_updated(true);
}

point_t
plane_t::origin()
{
    return m_plane_origin;
}

void
plane_t::operator+=(const rect_t &rect)
{
    m_plane += rect;
    set_updated(true);
}

void
plane_t::operator-=(const rect_t &rect)
{
    m_plane -= rect;
    set_updated(true);
}

void
plane_t::set_origin(const point_t &point)
{
    m_plane_origin = point;
    set_updated(true);
}

void
plane_t::reset_plane()
{
    m_plane = region_t();
    set_updated(true);
}
