//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM_REGION_FACTORY__H
#define VM_REGION_FACTORY__H

#include <window_manager.h>
#include <vm_region.h>

class vm_region_factory_t
{
public:
  vm_region_factory_t(window_manager_t &wm) : m_wm(wm)
  {

  }

  virtual ~vm_region_factory_t() = default;

  virtual std::shared_ptr<vm_region_t> make_vm_region(std::shared_ptr<vm_base_t> &vm) = 0;
  virtual void refresh_vm_region(uuid_t uuid)
  {
      m_wm.update_guest(uuid);
  }

protected:
  window_manager_t &m_wm;
};

#endif // VM_REGION_FACTORY__H
