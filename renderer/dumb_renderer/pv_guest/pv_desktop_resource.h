//
// Glass Display 
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PV_DESKTOP_RESOURCE__H
#define PV_DESKTOP_RESOURCE__H

#include <glass_types.h>

#include <framebuffer.h>
#include "pv_display_resource.h"

#include <QQueue>

extern "C" {
#include <pv_display_backend_helper.h>
};

#define MAX_DISPLAYS 6

namespace pv_port
{
  constexpr const auto pv_control_port = 1000;
  constexpr const auto qemu_control_port = 1500;
};

class pv_desktop_resource_t : public QObject
{
    Q_OBJECT

public:
    // These run in their own thread, we need to queue them back to the
    // Qt event loop thread to avoid BadStuff(TM).
    static void capabilities_request(struct pv_display_consumer *consumer,
                                     struct dh_driver_capabilities *request);
    static void advertise_list_request(struct pv_display_consumer *consumer,
                                       struct dh_display_advertised_list *advertised_list);
    static void display_no_longer_available_request(struct pv_display_consumer *consumer,
                                                    struct dh_display_no_longer_available *request);
    static void text_mode_request(struct pv_display_consumer *consumer,
                                  bool force);
    static void fatal_consumer_error(struct pv_display_consumer *consumer);

    static void new_control_channel_connection(void *opaque, struct libivc_client *client);

    pv_desktop_resource_t(uuid_t uuid,
                          domid_t domid,
                          desktop_plane_t *desktop,
                          uint32_t starting_port);
    ~pv_desktop_resource_t();

    dh_display_info *display_infos();

    bool qemu() { return m_starting_port == 1500; }
    uint32_t display_info_count() { return m_display_info_count; }
    void update_displays(desktop_plane_t *desktop);
    uuid_t uuid() { return m_uuid; }

public slots:
    void remove_display_slot(std::shared_ptr<pv_display_backend> pv_display,
                             uint32_t key,
                             qlist_t<uint32_t> port_list);

signals:
    void desktop_ready(bool ready);
    void valid_changed();
    void add_dirty_rect(rect_t rect);

    void move_cursor(uint32_t key, point_t position);
    void update_cursor(uint32_t key, point_t hot_spot, std::shared_ptr<cursor_t> cursor);
    void hide_cursor(uint32_t key);
    void render_source_plane_signal(uint32_t key);
    void blank_display_signal(bool isOff);
    void restore_qemu_signal();

private slots:
    void add_display(uint32_t key, uint32_t x, uint32_t y, uint32_t width, uint32_t height);
    void remove_display(uint32_t key);
    void enable_text_mode(bool force);
    void handle_error();
    void finish_control_connection(void *cli);
    void publish_display_list();

private:
    void initialize();
    void connect_display_resource_signals(pv_display_resource_t *display);
    void disconnect_display_resource_signals(pv_display_resource_t *display);
    void update_displays();

    uuid_t m_uuid;
    domid_t m_domid;
    uint32_t m_starting_port;

    QQueue<uint32_t> m_port_pool;

    std::shared_ptr<pv_display_consumer> m_pv_backend_consumer;
    desktop_plane_t *m_desktop;

    std::unique_ptr<dh_display_info[]> m_display_info;
    uint32_t m_display_info_count;
    uint32_t m_max_display_count;

    qmutex_t m_lock;

    bool m_blanked;

    Q_DISABLE_COPY(pv_desktop_resource_t);
};

#endif // PV_DESKTOP_RESOURCE__H
