QT       += core gui 

TARGET = test
TEMPLATE = app

CONFIG += c++17

SOURCES += main.cpp 

INCLUDEPATH += "../include"
INCLUDEPATH += "../../window_manager/include"
INCLUDEPATH += "../../toolstack/include"
INCLUDEPATH += "../../input/include"
INCLUDEPATH += "../../common/config"
INCLUDEPATH += "../../common"
INCLUDEPATH += "./"
INCLUDEPATH += "../../../pv-display-helper"
INCLUDEPATH += "../../../ivc_driver/include/core"

qt_renderer.path = /usr/lib
qt_renderer.files = libs/*
LIBS += -L. -ldumb_renderer -L/usr/local/lib -lpvbackendhelper -livc
INSTALLS += dumb_renderer
